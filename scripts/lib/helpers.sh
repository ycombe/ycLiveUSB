#!/bin/bash
TRANSFERT="live md5sum.txt"

# Convert sizes to another unit, output formatted for parted syntax (kiB for 1024B)
# Output is truncated, usefull to pass to a smaller unit.
# bc -l at end to get output not truncated.
convert_XiB(){
    bse="$2"
    case "$2" in
	"N")  unit=""
	    bse=""
	    ;;
	"B")  unit="B"
	    ;;
	"K") unit="kiB"
	    ;;
	"M") unit="MiB"
	    ;;
	"G") unit="GiB"
	    ;;
	*)  unit=""
	    bse=""
	    ;;
    esac
    echo print $(echo "($1)/(1$bse)" | sed -e "s/kiB/*1024/g;s/MiB/*1024*1024/g;s/GiB/*1024*1024*1024/g;s/B/*1/g;s/K/*1024/g;s/M/*1024*1024/g;s/G/*1024*1024*1024/g"),\"$unit\" | bc 
}
# convert_XiB 2000M 
# convert_XiB 2000M K

# Ask for confirmation.
# stolen on the web. where ?
YesNo(){
    while true; do
	if [ -n "$2" ]; then
	    LineOfStars 60
	    echo -e -n "$2"
	    LineOfStars 60
	fi
	read -p "$1 (Y/N)?" answer
	case $answer in
            [Yy]* ) echo "YES"; return 0;;
            [Nn]* ) echo "NO"; return 1;;
            * ) echo "Please answer yes or no.";;
	esac
    done
}
# YesNo "Sure" || exit 

 
# put a line of star to catch reader attention.
LineOfStars(){
    for i in `seq $1`; do echo -n '*'; done
    echo -e -n '\n'
    echo -n ""
}
# LineOfStars 50

# Check script is run as root
Checkroot(){
    if [ `whoami` != root ]; then
	echo "Not root !"
	return 1
    else
	return 0
    fi
}
# Checkroot || exit 1

# install grub on device
# $1 is device itself
# $2 is device partition mount point for grub/ directory 
Install_Grub(){
    echo -n "Installing bootloader GRUB on $1 ... "
    if ( LANG=C grub-install --force --recheck --no-floppy --modules="biosdisk part_gpt fat" --boot-directory="$2/boot/" "$1" 2>&1 | grep "No error reported." > /dev/null )  ; then
	echo OK
	return 0
    else
	echo "Error. Check error messages !"
	set -x
	mount
	df
	grub-install --force --recheck --no-floppy --modules="biosdisk part_gpt fat" --boot-directory="$2" "$1"
	set +x
	return 1
    fi
}

Clean(){
    echo -n "Syncing..."
    sync
    echo OK
    Umount_all $MOUNTDIRS

    Detach $LOOPS
    rm -fr TMPLIVE/
    if is_loop; then
	echo dmsetup remove $PART1 $PART2 ${PART2%?}1
	dmsetup remove $PART1 $PART2 ${PART2%?}1
	echo kpartx -d $FILEDEV
	kpartx -d $FILEDEV
    fi
}


ExitErr(){
    [ $# -ge 1 ] && echo "$@"
    echo "Aborting..."
    Clean
    exit 1
}

Attach(){
    SIZELIMIT=""
    if [ "$4" != "ALL" ]; then
	SIZELIMIT="--sizelimit $4 "
    fi 
    echo losetup -f -o "$2" $SIZELIMIT --show "$1"
    eval "$3"=$(losetup -f -o "$2" $SIZELIMIT --show "$1")
    LOOPS="$LOOPS $(eval echo \$$3)"
}


Detach(){
    if [ $# -ne 0 ]; then
	echo -n "Detaching loop devices ... "
    	sleep 1
	while [ -n "$1" ]; do
	    echo -n "$1 "
    	    losetup -v -d $1
	    shift
	done
	echo OK
    fi
}

# Warning. Must have been mount on temp dir
Umount_all(){
    if [ $# -ne 0 ]; then
	echo -n "Umounting and removing temporary directory ... "
	umount  $@
	rmdir  $@
	echo OK
    fi
}

# source 
MountOnTemp()
{
    ( /sbin/fsck -N $1  | grep "$2" 2>&1 > /dev/null ) || ExitErr "Partiton $PERSISTDEV is not an $2 partition !"

    eval "$3"=$(basename `mktemp -d --tmpdir=. $3.XXXX`)

    MOUNTDIRS="$MOUNTDIRS $(eval echo \$$3)"

    mount "$1" $(eval echo "\$$3") > /dev/null
}

Install_Live(){
    echo  "Copying live system files ... "
    [ -d "$2/live" ] || mkdir "$2/live"
    for target in $TRANSFERT; do
	echo -n "$file "
	rsync -avc --progress  $1/$target $2
    done
    echo "Copying live system files " DONE
}

CheckLive(){
    ( [ -d "$1/live" ] && [ -f "$1/live/filesystem.squashfs" ] ) || ExitErr $1 is not a live image ! 
}
