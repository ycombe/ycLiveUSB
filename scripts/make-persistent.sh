#!/bin/bash

LIBDIR="${0%/*}/lib"
for FILE in `ls ${LIBDIR}/*.sh`; do
    . ${FILE}
done

# Size in KiB for the offset of the first partition.
# Should be an erase block multiple.
STARTPART=1M
BGSIZE=2M
#size of erase block in 4K blocks
ERASE4KBLOCKS=1

debug_vars() {
    for var in "$@"
    do
	eval echo $var=\$$var
    done
}

Usage()
{
    echo "Usage :"
    echo "   $0 KEY SIZE-PART1  IMAGE"
    echo -n -e "\n"
    echo "Example:"
    echo "   $0 /dev/sdb 2000M binary.tar"
    echo -n -e "\n"
    echo "   Make two (aligned) partitions for Debian live"
    echo "     and install IMAGE live system files on it."
    echo "      - first partition will be FAT formatted for live system,"
    echo "      - second partition will be  ext4 NON-JOURNALED formatted"
    echo "          for persistence data."
    echo -n -e "\n"
    echo "    The KEY (device or file image) will be erased."
    echo "    SIZE-PART1 is the size of the first partition."
    echo "    The second partition will fit the rest of KEY."
    echo -n -e "\n"
  exit 1
}

getLine ()
{
    head -n $@ | tail -1 ;
}

GetDeviceSize(){
    echo -n "Looking for exact size of $1 ..."
    KSIZE=`blockdev --getsize64 "$1"`
    echo $KSIZE
}


TestNumber(){
    nbr=$(convert_XiB "$1")
  [ "$nbr" -eq "$nbr" ] 2>/dev/null
}

Check_LiveImage(){
    echo -n "Checking live image file ... "
    [ -f "$1" ] || ExitErr "Live image file $1 does not exists !"

    mkdir TMPLIVE/

    tar -xf "$1" -C TMPLIVE/    

    LIVEDIR="TMPLIVE/binary"
    CheckLive "$LIVEDIR"
    echo OK
}

CheckSizes(){
    echo -n "Checking partitions sizes ... "
    [ "$(convert_XiB $PSIZE N)" -lt "$(convert_XiB $KSIZE N)" ] || ExitErr "Partition size $2 too big ! should no more than" $(convert_XiB "$KSIZE-$STARTPART" M) " !"
    echo $(convert_XiB $PSIZE M ) and $(convert_XiB "$KSIZE-$STARTPART-$PSIZE" M)
}


Checkparms(){
    [ $# -eq 3 ] || Usage

    ISLOOP=${1%?}

    if [ -b "$1" ] && [ "$ISLOOP" != "/dev/loop" ]; then
	DEVICE=$1
	is_loop() { false ; }
    elif [ -f "$1" ] || [ "$ISLOOP" = "/dev/loop" ]; then
	DEVICE=$(losetup -f --show $1)
	is_loop() { true ; }
	FILEDEV=$1
    else
	ExitErr "$1 not device nor file !"
    fi
    PSIZE=$2
    LIVEIMAGE=$3

    TestNumber "$PSIZE" || Usage

    ( mount | grep "$DEVICE" > /dev/null ) && ExitErr "$DEVICE is mounted !"
    
    Check_LiveImage "$LIVEIMAGE"

    GetDeviceSize  "$DEVICE"

    CheckSizes
}

Partitions(){
    echo -n "Partitionning ... "
    BGSTART=$(convert_XiB "$2" "K")
    BGEND=$(convert_XiB "$2 + $BGSIZE" "K")
    START=$(convert_XiB "$BGEND" "K")
    END=$(convert_XiB "$BGEND + $3" "K")
    STARTPOS=$(convert_XiB "$BGEND" "N")
    ENDPOS=$(convert_XiB "$BGEND + $3" "N")
    
    #debug_vars BGSTART BGEND START END STARTPOS ENDPOS

    parted $1 --script \
	   unit KiB \
	   mklabel gpt \
	   mkpart primary fat32 "$BGSTART" "$BGEND" \
           mkpart primary fat32 "$START" "$END" \
	   mkpart primary ext4 "$END" 100% \
	   toggle 1 bios_grub \
    	   toggle 2 boot  || ExitErr "Error. Check error messages !"
    echo "OK"

    if is_loop; then
	losetup -d $DEVICE

	# kpartx fait maintenant n'importe quoi avec le -l

	KPARTXRES=$(kpartx -u -v $FILEDEV | sed -e 's/$/\\n/')
	
	PART1=/dev/mapper/$(echo -n -e $KPARTXRES | head -2 | tail -1 | awk '{print $3}')
        PART2=/dev/mapper/$(echo -n -e $KPARTXRES | tail -1 | awk '{print $3}')

	DEVICEMAIN=$(losetup -j $FILEDEV | awk  -F:  '{print $1 ;}')

	debug_vars DEVICEMAIN PART1 PART2

	#ExitErr "on sait jamais"

	#  NE MARCHE PLUS. 
	#PART1="/dev/mapper/$(kpartx -l $FILEDEV |  awk '/^loop.p2/{ print $1 ;}')"
	#PART2="/dev/mapper/$(kpartx -l $FILEDEV |  awk '/^loop.p3/{ print $1 ;}')"
	#DEVICEMAIN="$(kpartx -l $FILEDEV |  awk '/^loop.p1/{ print $5 ;}')"

    else
	# part 1 is bios_grub
	DEVICEMAIN="$1"
	PART1="$DEVICE"2
	PART2="$DEVICE"3
    fi

    sleep 1 # wait for automount
    mount | grep "$DEVICE" > /dev/null && ExitErr "$DEVICE is mounted ! Close file manager (automount is active) !"

    echo -n "Formatting $PART1 as VFAT ... "
    # echo mkfs.vfat -s 8 -S 512 -n "DEBIAN_LIVE" "$PART1" 
    mkfs.vfat -s 8 -S 512 -n "DEBIAN_LIVE" "$PART1" > /dev/null
    echo OK

    echo -n "Formatting $PART2 as (non journaled) ext4 ... "
    mkfs.ext4 -b 4096 -E stride="$ERASE4KBLOCKS",stripe-width="$ERASE4KBLOCKS" -O ^has_journal -L "persistence" "$PART2" &> /dev/null
    echo OK

}

Mount_all(){
    echo -n "Mounting partitions on temporary directories ... "
    MountOnTemp "$1" vfat PART1DIR
    MountOnTemp "$2" ext4 PART2DIR
    echo OK
}


Install_persistence(){
    cat > "$1/persistence.conf" <<EOF
/ union
EOF
    FILE_CONTENTS=persistence.tar.gz
    [ -f $FILE_CONTENTS ] && echo -n "Installing $FILE_CONTENTS in persistence... "
    [ -f $FILE_CONTENTS ] && cp $FILE_CONTENTS $1/
    [ -f $FILE_CONTENTS ] && echo OK
}

Checkroot || ExitErr "Aborting..."

Checkparms $@

if is_loop; then
    if ! YesNo Continuer "\t\t\tWarning !\n\t$FILEDEV will be erased !\n"; then
	Clean
	losetup -d $DEVICE
	exit 0
    fi
else
    if ! YesNo Continuer "\t\t\tWarning !\n\t$DEVICE will be erased !\n"; then
	Clean
	exit 0
    fi
fi

Partitions "$DEVICE" "$STARTPART" "$PSIZE"

Mount_all "$PART1" "$PART2"

debug_vars DEVICEMAIN PART1DIR LIVEDIR

# Install at first the PC version of grub
apt-get -y  install grub-pc
Install_Grub "$DEVICEMAIN" "$PART1DIR" || ExitErr "Aborting !"

# then install the efi version
apt-get -y  install  grub-efi-amd64
echo Echo installing efi boot system...
echo grub-install --removable --efi-directory="$PART1DIR" --target=x86_64-efi -v --boot-directory="$PART1DIR/boot/" --no-bootsector "$DEVICEMAIN"
grub-install --removable --efi-directory="$PART1DIR" --target=x86_64-efi -v --boot-directory="$PART1DIR/boot/" --no-bootsector "$DEVICEMAIN"

UUID=$(grub-probe -t fs_uuid -d "$PART1")

find "$LIVEDIR"/boot/grub/ -mindepth 1 -maxdepth 1 -not -name i386-pc -a -not -name live-theme -exec cp -av  '{}' "$PART1DIR"/boot/grub/ \;
cp -av live-theme/  "$PART1DIR"/boot/grub/
cp -v /usr/share/grub/unicode.pf2 "$PART1DIR"/boot/grub/

sed -i -e "s/set default=0/set default=0\nsearch --no-floppy --fs-uuid --set=root ${UUID}\nset prefix=(\$root)\/boot\/grub\/\nset timeout=10/" "$PART1DIR"/boot/grub/grub.cfg

# debug if necessary
# cp -vf "$PART1DIR"/boot/grub/grub.cfg .

Install_persistence "$PART2DIR"

Install_Live "$LIVEDIR" "$PART1DIR"

Clean
