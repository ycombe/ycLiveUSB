#!/bin/bash

PROXY_CFG=/etc/proxy-cfg
ENVIRONMENT=/etc/environment

#PROXY_CFG=proxy-cfg
#ENVIRONMENT=environment

# stolen on the ineternt :
# http://stackoverflow.com/questions/13777387/check-for-ip-validity
check_ip ()
{
if expr "$1" : '[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*$' >/dev/null; then
  for i in 1 2 3 4; do
    if [ $(echo "$1" | cut -d. -f$i) -gt 255 ]; then
      return 1
    fi
  done
  return 0
else
  return 1
fi
}

check_port ()
{
    expr $1 \> 1024 \& $1 \< 10000  > /dev/null;
    return $?
}

ask_proxy ()
{
    result=1
    IPCHECK=""
    PORTCHECK=""
    while [ $result -ne 0  ] ; do
	proxy=$(zenity --forms --title="$1" --text="\nAdresse IP: quelque chose comme 10.0.0.254 (quatre\nnombres entre 0 et 255 séparés par des points).\n\nPort: nombre entre 1025 et 9999${IPCHECK}${PORTCHECK}" --separator=":" --add-entry="Adresse IP" --add-entry="Port")
	if [ "$?" = "1" ]; then
	    exit 0
	fi
	ip=${proxy%:*}
	port=${proxy#*:}
	IPCHECK=""
	PORTCHECK=""
	check_ip $ip || IPCHECK="\n\n<span foreground=\"red\" size=\"x-large\">IP \"${ip}\" incorrecte.</span>"
	check_port $port || PORTCHECK="\n\n<span foreground=\"red\" size=\"x-large\">Port \"${port}\" incorrect.</span>"
	[ -z "${IPCHECK}${PORTCHECK}" ]
	result=$?
    done
}

remove_proxy_cfg ()
{
    sudo sed -i '/\# PROXY_CFG/,$d' ${ENVIRONMENT}
}

put_proxy_cfg ()
{
    remove_proxy_cfg

    sudo sh -c "cat <<EOF >> ${ENVIRONMENT}
# PROXY_CFG
# Do not change anything in this part.
# Generated part by a parameter script.
# Changes will be lost
#

EOF"

    sudo sh -c "cat ${PROXY_CFG} >>  ${ENVIRONMENT}"
}

ask_restart_session ()
{
    zenity  --title="Redémarrage de la session" --question  --ok-label="Redémarrer" --cancel-label="Annuler" --text="
<big><b>Redémarrage de la session</b></big>

Pour que les podifications soient prise en compte,
il faut redémarrer la session.

Cela va fermer toutes les applications ouvertes. 
Enregistrer maintenant vos données si besoin.
"

if [ $? -eq 1 ]; then
    zenity --title="Annulation du redémarrage" --info --text="
Les modifications seront prises en compte au prochain démarrage.
"
else
    sudo service slim restart
fi
}

parametrage ()
{
    zenity  --title="Paramétrage du proxy" --question  --ok-label="D'accord, on y va" --cancel-label="Annuler" --text="
<big><b>Paramétrage du proxy</b></big>\n 

Le paramètrage du proxy utilise une adresse IP et un port.

L'adresse IP est constituée de quatre nombres entre 0 et 255,
comme 10.0.0.254 ou 192.168.0.25 par exemple.

Le port est un nombre entre 1024 et 9999.

Une fois les mdoifications faites, une confirmation du redémarrage 
de la session vous sera demandée pour que les modifications 
soient prises en compte par tous les programmes. 

<b>Cela fermera tous les programmes en activité.</b>

Vous pouvez paramétrer le proxy et ne pas redémarrer la session,
dans ce cas les modifications ne seront prises en compte qu'au 
prochain démarrage.
"

if [ $? -eq 1 ]; then
    exit 0
fi

SHASUM_ORIG=$(sha1sum <  ${PROXY_CFG} | awk '{print $1;}')

ask_proxy "Paramétrage du proxy"

sudo sed -i -e "s/^http_proxy=*$/http_proxy=http:\/\/${proxy}\//" ${PROXY_CFG}
sudo sed -i -e "s/^https_proxy=*$/https_proxy=http:\/\/${proxy}\//" ${PROXY_CFG}
sudo sed -i -e "s/^ftp_proxy=*$/ftp_proxy=http:\/\/${proxy}\//" ${PROXY_CFG}
sudo sed -i -e "s/^HTTP_PROXY=*$/HTTP_PROXY=http:\/\/${proxy}\//" ${PROXY_CFG}
sudo sed -i -e "s/^HTTPS_PROXY=*$/HTTPS_PROXY=http:\/\/${proxy}\//" ${PROXY_CFG}
sudo sed -i -e "s/^FTP_PROXY=*$/FTP_PROXY=http:\/\/${proxy}\//" ${PROXY_CFG}

SHASUM_NEW=$(sha1sum <  ${PROXY_CFG} | awk '{print $1;}')

if [ "$SHASUM_ORIG" = "$SHASUM_NEW" ]; then
    zenity --title="Annulation" --info --text="\
Le fichier n'a pas été modifié.

Pas de redémarrage de la session.
"
    exit 0
else
    put_proxy_cfg
    
    ask_restart_session
    exit 0
fi

}

parametrage_separe ()
{
    zenity  --title="Paramétrage des proxys http, https et ftp" --question  --ok-label="D'accord, on y va" --cancel-label="Annuler" --text="
<big><b>Paramétrage des proxys http, https et ftp</b></big>\n 

Le paramètrage de chaque proxy utilise une adresse IP et un port.

L'adresse IP est constituée de quatre nombres entre 0 et 255,
comme 10.0.0.254 ou 192.168.0.25 par exemple.

Le port est un nombre entre 1024 et 9999.


Une fois les mdoifications faites, une confirmation du redémarrage 
de la session vous sera demandée pour que les modifications 
soient prises en compte par tous les programmes. 

<b>Cela fermera tous les programmes en activité.</b>

Vous pouvez paramétrer le proxy et ne pas redémarrer la session,
dans ce cas les modifications ne seront prises en compte qu'au 
prochain démarrage.
"

if [ $? -eq 1 ]; then
    exit 0
fi

SHASUM_ORIG=$(sha1sum <  ${PROXY_CFG} | awk '{print $1;}')

ask_proxy "Paramétrage du proxy http"

sudo sed -i -e "s/^http_proxy=*$/http_proxy=http:\/\/${proxy}\//" ${PROXY_CFG}
sudo sed -i -e "s/^HTTP_PROXY=*$/HTTP_PROXY=http:\/\/${proxy}\//" ${PROXY_CFG}

ask_proxy "Paramétrage du proxy https"
sudo sed -i -e "s/^https_proxy=*$/https_proxy=http:\/\/${proxy}\//" ${PROXY_CFG}
sudo sed -i -e "s/^HTTPS_PROXY=*$/HTTPS_PROXY=http:\/\/${proxy}\//" ${PROXY_CFG}

ask_proxy "Paramétrage du proxy ftp"
sudo sed -i -e "s/^ftp_proxy=*$/ftp_proxy=http:\/\/${proxy}\//" ${PROXY_CFG}
sudo sed -i -e "s/^FTP_PROXY=*$/FTP_PROXY=http:\/\/${proxy}\//" ${PROXY_CFG}

SHASUM_NEW=$(sha1sum <  ${PROXY_CFG} | awk '{print $1;}')

if [ "$SHASUM_ORIG" = "$SHASUM_NEW" ]; then
    zenity --title="Annulation" --info --text="\
Le fichier n'a pas été modifié.

Pas de redémarrage de la session.
"
    exit 0
else
    put_proxy_cfg
    
    ask_restart_session
    exit 0
fi

}

case $1 in
    disable)
	remove_proxy_cfg
	exit 0
	;;
    enable)
	put_proxy_cfg
	exit O
	;;
esac

action=$(zenity --list   --title="Choisissez l'action à effectuer" --text="" --column "Actions"  "Supprimer le paramétrage existant" "Restaurer la configuration supprimée précédemment" "Paramétrer un proxy" "Paramétrer des proxys séparés http/https/ftp" "Quitter" --width=400 --height=250 )

if [ "$?" = "1" ]; then
    exit 0
fi

case $action in
    "Quitter")
	exit 0
	;;
    "Supprimer le paramétrage existant")
	SHASUM_ORIG=$(sha1sum <  ${ENVIRONMENT} | awk '{print $1;}')
	remove_proxy_cfg
	SHASUM_NEW=$(sha1sum <  ${ENVIRONMENT} | awk '{print $1;}')
	if [ "${SHASUM_ORIG}" != "${SHASUM_NEW}" ]; then
	    ask_restart_session
	else
	    zenity --title="Annulation" --info --text="\
Le fichier n'a pas été modifié.

Pas de redémarrage de la session.
"
	fi
	exit 0
	;;
    "Restaurer la configuration supprimée précédemment")
	SHASUM_ORIG=$(sha1sum <  ${ENVIRONMENT} | awk '{print $1;}')
	put_proxy_cfg
	SHASUM_NEW=$(sha1sum <  ${ENVIRONMENT} | awk '{print $1;}')
	if [ "${SHASUM_ORIG}" != "${SHASUM_NEW}" ]; then
	    ask_restart_session
	else
	    zenity --title="Annulation" --info --text="\
Le fichier n'a pas été modifié.

Pas de redémarrage de la session.
"
	fi
	;;
    "Paramétrer un proxy")
	parametrage
	;;
    "Paramétrer des proxys séparés http/https/ftp")
	parametrage_separe
	;;
esac
