# ycLiveUSB

## Aim
Keep configurations files used to generate my debian live USB Key.

This live usb is [enlightenment](http://www.enlightenment.org) based, with some  office and  math tools.

## Needs
From a debian instalaltion, you need live-build package.

The LIST_packages.chroot file lists the deb files manually added in config/packages.chroot.
* Actually, the slim deb come from debian sid.
* Enlightenment files are manually made, you can replace them by xfce or something like that. Add the package you want in config/package-lists/desktop.list.chroot  

# Usage
## Preparation of the live image (tar format)
Just run (as root):

    lb clean
    lb config
    lb build

Image generated will be tar file, named live-image-amd64.tar.tar. I extract it and push the files in USB key by a script, to make EFI boot works.

## To make an image file of the key
Prepare an big file (4G):

    dd if=/dev/zero of=key.img bs=1M count=4000
Partition it, install bootloader and copy the files:

    scripts/make-persistent.sh key.img 2000M live-image-amd64.tar.tar
2000M is the size of the VFAT partition for the live system. The persistence partition get the remaining place.


## To make directly a bootable usb key
    scripts/make-persistent.sh /dev/sdb 2000M live-image-amd64.tar.tar
Make sure /dev/sdb is the device name of the key. This device will be erased.
2000M is the size of the VFAT partition for the live system. the persistence partition get the remaining place.

Use at your own risk. Normally, only the last command destroys your data, but your configuration is not mine.
